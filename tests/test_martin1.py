import unittest
from soundops import soundadd
from mysound import Sound


class TestSoundAdd(unittest.TestCase):

    def test_misma_longitud(self):
        s1 = Sound(7)
        s2 = Sound(7)
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 7)
