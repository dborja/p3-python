import unittest
from soundops import soundadd
from mysound import Sound


class TestSoundAdd(unittest.TestCase):

    def test_same_length(self):
        s1 = Sound(5)
        s2 = Sound(5)
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 5)

    def test_different_lengths(self):
        s1 = Sound(3)
        s2 = Sound(5)
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 5)

    def test_empty_sound(self):
        s1 = Sound(0)
        s2 = Sound(3)
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 3)


if __name__ == '__main__':
    unittest.main()
