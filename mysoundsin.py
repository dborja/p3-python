import math
from mysound import Sound


class SoundSin(Sound):

    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)
        self.frequency = frequency
        self.amplitude = amplitude

        for nsample in range(self.nsamples):
            t = nsample / self.samples_second
            self.buffer[nsample] = int(amplitude
                                       * math.sin(2 * math.pi * frequency * t))
