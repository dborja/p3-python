from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    result = Sound(max(s1.duration, s2.duration))

    for i in range(min(len(s1.buffer), len(s2.buffer))):
        result.buffer[i] = s1.buffer[i] + s2.buffer[i]

    if len(s1.buffer) > len(s2.buffer):
        result.buffer[len(s2.buffer):] = s1.buffer[len(s2.buffer):]
    else:
        result.buffer[len(s1.buffer):] = s2.buffer[len(s1.buffer):]

    return result
